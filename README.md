
# Take Home Project Satriags

Project ini memakai Framework Laravel sebagai Backend API dan Memakai NextJS Chakra UI sebagai Front End pada project ini.


## Cara Menjalankan Project

Lakukan clone project pada branch "main"
## Instalasi Laravel

Buka pada project "backend" dan jalankan command 

```bash
  composer install
```
Setelah selesai, silahkan melakukan configurasi database dengan mengganti nama file ".env.example" menjadi ".env" dan sesuaikan nama database yang anda inginkan.

Selanjutnya jalankan command dibawah ini :
```bash
php artisan storage:link && php artisan key:generate && php artisan migrate && php artisan db:seed && php artisan serve
```
Command diatas akan membuka permission public storage, membuatkan "API_KEY" pada file ".env", melakukan migrasi database, mengisi tabel database, dan menjalankan project Laravel dengan URL berikut :
```bash
http://127.0.0.1:8000
```
## Instalasi Next JS
Buka pada project "backend" dan jalankan command
```bash
npm install
```
setelah Instalasi selesai, jalankan project nextjs dengan command berikut 
```bash
npm run dev
```
Project dapat dijalankan dengan URL berikut :
```bash
http://localhost:3001
```

## Informasi API
## API POSTS
LIST DATA POSTS [GET]
```bash
http://127.0.0.1:8000/api/posts
```

CREATE POSTS [POST] {image:file.png,title:'title',content:'content'}
```bash
http://127.0.0.1:8000/api/posts
```
DETAIL POSTS [GET]
```bash
http://127.0.0.1:8000/api/posts/(:id)
```

UPDATE POST [POST]
{image:file.png,title:'title',content:'content'}
```bash
http://127.0.0.1:8000/api/posts/(:id)
```

DELETE POST [DELETE]
```bash
http://127.0.0.1:8000/api/posts/(:id)
```
