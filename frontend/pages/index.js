import Head from "next/head";
import NextLink from 'next/link';
import axios from "axios";
import { useRouter } from 'next/router';
import { Link, Text, Image, Box, Grid, GridItem, Heading, Flex, Button, Stack, Container, VStack, Table, Thead, Tbody, Tr, Th, Td, chakra, Tab } from "@chakra-ui/react";

export async function getServerSideProps(context) {
  //http request
  var id = context.query["page"];
  // router.replace(router.asPath);
  var pageUrl = "";
  if(id == "") {
    pageUrl = '';
  } else {
    pageUrl = '?page=' + id;
  }

  const req = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/posts` + pageUrl)
  const res = await req.data.data.data;
  const page = await req.data.data.links;
  return {
    props: {
      posts: res, // <-- assign response
      page: page // <-- assign response
    },
  }
}


function PostsIndex(props) {
  const posts = props['posts'];
  const page = props['page'];
  const router = useRouter();

  const refreshData = () => {
    router.replace(router.asPath);
  }

  const pageData = async (id) => {
    refreshData();
  }

  const deletePost = async (id) => {

    //sending
    await axios.delete(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/posts/${id}`);

    //refresh data
    refreshData();

  }
  return (
    <>
      <Head>
        <title>Next App with Chakra UI</title>
        <meta name="description" content="POSTS" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Flex alignItems="center" justifyContent="center">

          <Heading>Daftar Post</Heading>

        </Flex>

        <Container maxW="4xl" color='white'>
          <Link padding="2" rounded={5} margin="1" href="#" color='white' background='black' href="/create">
            TAMBAH DATA
          </Link>
          {posts.map((post) => (

            <Box p={4} display={{ md: 'flex' }}>

              <Box flexShrink={0}>
                <Image
                  borderRadius='lg'
                  width={{ md: 40 }}
                  src={`${process.env.NEXT_PUBLIC_API_BACKEND}/storage/posts/${post.image}`}
                  alt='Woman paying for a purchase'
                />
              </Box>
              <Box mt={{ base: 4, md: 0 }} ml={{ md: 6 }}>
                <Link
                  mt={1}
                  display='block'
                  fontSize='lg'
                  lineHeight='normal'
                  fontWeight='semibold'
                  href={`/detail/${post.id}`}>
                  <Text
                    fontWeight='bold'
                    textTransform='uppercase'
                    fontSize='xl'
                    letterSpacing='wide'
                    color='teal.600'
                  >
                    {post.title}

                  </Text>


                  <Text mt={2} color='gray.500'>
                    Detail Post
                  </Text>



                </Link>
                <Link float="right" onClick={() => deletePost(post.id)} padding="2" rounded={5} margin="1" href="#" color='white' background='black'>Hapus</Link>
                <Link float="right" padding="2" rounded={5} margin="1" href={`/edit/${post.id}`} color='white' background='black'>Edit</Link>
              </Box>
            </Box>
          ))}
          <br></br>
          {page.map((page) => (
            <Link onClick={() => pageData(page.label)} as={NextLink} padding="2" rounded={5} margin="1" href={`?page=${page.label}`} color='white' background='black'>{page.label}</Link>
          ))}
        </Container>


      </main>

    </>
  );
}

export default PostsIndex;