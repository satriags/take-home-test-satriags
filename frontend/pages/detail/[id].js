//import hook useState
import { useState } from 'react';
import Head from "next/head";
import NextLink from 'next/link';
import axios from "axios";
import { useRouter } from 'next/router';
import { Link, Text, Image, Box, Grid, GridItem, Heading, Flex, Button, Stack, Container, VStack, Table, Thead, Tbody, Tr, Th, Td, chakra, Tab } from "@chakra-ui/react";
//fetch with "getServerSideProps"
export async function getServerSideProps({ params }) {

    //http request
    const req = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/posts/${params.id}`)
    const res = await req.data.data

    return {
        props: {
            post: res // <-- assign response
        },
    }
}

function PostDetail(props) {

    //destruct
    const { post } = props;

    return (
        <>
            <Head>
                <title>Next App with Chakra UI</title>
                <meta name="description" content="POSTS" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <Flex alignItems="center" justifyContent="center">

                    <Heading>Detail Post</Heading>
                </Flex>
                <Container maxW="4xl" color='white'>

                    <Box flexShrink={0}>
                        <Image
                            borderRadius='lg'
                            width='100%'
                            src={`${process.env.NEXT_PUBLIC_API_BACKEND}/storage/posts/${post.image}`}
                            alt='Woman paying for a purchase'
                        />
                    </Box>
                    <Box Box p={4} display={{ md: 'flex' }}>


                        <Box mt={{ base: 4, md: 0 }} ml={{ md: 6 }}>

                            <Text
                                fontWeight='bold'
                                textTransform='uppercase'
                                fontSize='xl'
                                letterSpacing='wide'
                                color='teal.600'
                            >
                                {post.title}
                            </Text>

                            Finding customers for your new business

                            <Text mt={2} color='gray.500'>
                                {post.content}
                            </Text>
                            <br></br>
                            <Link padding="2" rounded={5} margin="1" href="/" color='white' background='black'>Kembali</Link>

                        </Box>
                    </Box>

                </Container>


            </main>

        </>

    );

}

export default PostDetail