//import hook useState
import { useState } from 'react';
import Head from "next/head";
import NextLink from 'next/link';
import axios from "axios";
import Router from 'next/router';
import { useRouter } from 'next/router';
import { Link, Text, Image, Box, Grid, GridItem, Heading, Flex, Button, Stack, Container, VStack, Table, Thead, Tbody, Tr, Th, Td, chakra, Tab, Input, Textarea } from "@chakra-ui/react";
//fetch with "getServerSideProps"
export async function getServerSideProps({ params }) {

    //http request
    const req = await axios.get(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/posts/${params.id}`)
    const res = await req.data.data

    return {
        props: {
            post: res // <-- assign response
        },
    }
}

function PostEdit(props) {

    //destruct
    const { post } = props;

    //state
    const [image, setImage] = useState("");
    const [title, setTitle] = useState(post.title);
    const [content, setContent] = useState(post.content);

    //state validation
    const [validation, setValidation] = useState({});

    //function "handleFileChange"
    const handleFileChange = (e) => {

        //define variable for get value image data
        const imageData = e.target.files[0]

        //check validation file
        if(!imageData.type.match('image.*')) {

            //set state "image" to null
            setImage('');

            return
        }

        //assign file to state "image"
        setImage(imageData);
    }

    //method "updatePost"
    const updatePost = async (e) => {
        e.preventDefault();

        //define formData
        const formData = new FormData();

        //append data to "formData"
        formData.append('image', image);
        formData.append('title', title);
        formData.append('content', content);
        formData.append('_method', 'PUT');

        //send data to server
        await axios.post(`${process.env.NEXT_PUBLIC_API_BACKEND}/api/posts/${post.id}`, formData)
            .then(() => {

                //redirect
                Router.push('/')

            })
            .catch((error) => {

                //assign validation on state
                setValidation(error.response.data);
            })

    };
    return (
        <>
            <Head>
                <title>Next App with Chakra UI</title>
                <meta name="description" content="POSTS" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <Flex alignItems="center" justifyContent="center">

                    <Heading>Edit Post</Heading>
                </Flex>
                <Container maxW="4xl" color='white'>
                    <form onSubmit={updatePost}>

                        <Box flexShrink={0}>
                            <Image
                                borderRadius='lg'
                                width='100%'
                                src={`${process.env.NEXT_PUBLIC_API_BACKEND}/storage/posts/${post.image}`}
                                alt='Woman paying for a purchase'
                            />
                        </Box>
                        <Box Box p={4} display={{ md: 'flex' }}>


                            <Box width="100%" mt={{ base: 4, md: 0 }} ml={{ lg: 6 }}>
                                <Text
                                    fontWeight='bold'
                                    textTransform='uppercase'
                                    fontSize='xl'
                                    letterSpacing='wide'
                                    color='teal.600'
                                >
                                    <label >Image</label>
                                    <Input type="file" onChange={handleFileChange} />
                                </Text>

                                <Text
                                    fontWeight='bold'
                                    textTransform='uppercase'
                                    fontSize='xl'
                                    letterSpacing='wide'
                                    color='teal.600'
                                >
                                    <label >TITLE</label>
                                    {
                                        validation.title &&
                                        <Text color='red'>
                                            *{validation.title}
                                        </Text>
                                    }
                                    <Input type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Masukkan Title" />

                                </Text>


                                <Text textTransform='uppercase'
                                    fontSize='xl'
                                    color='teal.600' fontWeight='bold' mt={2} >
                                    <label >Content</label>
                                    {
                                        validation.content &&
                                        <Text color="red">
                                            *{validation.content}
                                        </Text>
                                    }
                                    <Textarea height="400" rows={3} value={content} onChange={(e) => setContent(e.target.value)} placeholder="Masukkan Content" />

                                </Text>
                                <br></br>
                                <Button colorScheme="blue" type="submit">   UPDATE
                                </Button>
                                <Link float="right" padding="2" rounded={5} href="/" color='white' background='black'>Batal Edit</Link>

                            </Box>
                        </Box>
                    </form>
                </Container>


            </main>

        </>

    );

}

export default PostEdit