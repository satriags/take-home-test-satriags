<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $dataPost =
            [
                [
                    'title' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit magni reprehenderit dignissimos non. Illo laboriosam qui, odio explicabo animi enim.',
                    'image' => 'aF8sNM8iVCcnpEnuOAcuLuMuhBz9MpUBCHnZPvP1.png',
                    'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt numquam reprehenderit, molestias illo maxime nam totam consequuntur quibusdam autem inventore eligendi. Corrupti soluta assumenda esse illum eveniet quis modi, est, dolor commodi, ut sapiente. Temporibus ab tempore illo tenetur commodi earum dignissimos dolor enim nobis distinctio, natus delectus quam nulla, debitis itaque, rerum adipisci perspiciatis quas perferendis est totam assumenda eius aut mollitia. Autem modi iure, provident perferendis, quibusdam, consequatur cum voluptatem exercitationem cupiditate excepturi voluptate? Eum magni ducimus molestias non assumenda id eaque aspernatur nisi, sit fugit. Iste beatae excepturi omnis, optio labore dolorum amet asperiores voluptatum temporibus. Neque omnis, vero cum magni debitis ipsa assumenda quibusdam distinctio odio veritatis exercitationem laboriosam quis officiis quasi iste hic quia. Nam dicta temporibus excepturi possimus sapiente perspiciatis doloremque nostrum, quisquam sequi minus placeat accusamus ut sint, quos sunt fugiat deleniti nisi obcaecati ipsa consectetur aspernatur soluta quam incidunt tempore? Dignissimos quia dolores cum labore, illum excepturi sequi eum illo enim itaque! Cum ab architecto sint? Perspiciatis libero quos laudantium vero accusamus iusto, dicta blanditiis repellendus non vel expedita. Nulla beatae repellendus error ipsam doloribus explicabo sunt distinctio libero veritatis aliquid dignissimos quod nostrum, ex, asperiores tempora deserunt? Similique, voluptas deleniti omnis doloremque nesciunt repellat officiis nemo eaque impedit, numquam accusamus quidem esse? Doloremque officiis unde ipsam deserunt ducimus ex expedita excepturi voluptate quisquam. Laborum, facere expedita eum aut earum est consequuntur praesentium molestias quibusdam explicabo similique natus pariatur doloribus iste. Possimus provident aperiam debitis in. Cumque dolorem magni repudiandae ullam qui!',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, sint.',
                    'image' => 'aFBgZTVumGWLcs2XRTddH1CQZcK96NTSA7loWJL5.jpg',
                    'content' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Officia repellat adipisci nisi possimus minus optio! Similique, odit cumque tenetur veritatis ipsa saepe repellendus amet dicta voluptatum aspernatur quae possimus id repudiandae ea deserunt, sint placeat ad molestias nulla voluptatem officiis eaque cum nesciunt maxime! Iure perspiciatis maiores at eaque earum quo ipsum? Sint voluptatum vitae vero, provident fuga repudiandae! Ullam ipsam amet ipsa autem sequi facilis quae, fugit maiores in!',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Lorem ipsum dolor sit amet.',
                    'image' => 'ZKfzI6AroDxp0ZGyBGI7UztjQHyxNmoCgaFF89Sn.png',
                    'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure non rem blanditiis! Deserunt soluta, dolore voluptate rerum corporis ducimus reiciendis temporibus autem officiis labore fugiat quas quod dolores porro doloremque, minus quibusdam voluptas. Reprehenderit, distinctio id. In, sequi? Atque dicta praesentium blanditiis architecto nulla eius mollitia eum accusantium alias saepe labore harum delectus, excepturi odit ipsa fuga quae, ut sint. Accusamus eveniet obcaecati, totam error ab amet dolor adipisci eligendi tempore! Quam hic doloribus, quae tempora ipsa fugit odit nam rerum incidunt, ex sint fugiat cupiditate aliquam quasi molestias corrupti?',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolores magnam iusto repellendus repellat consectetur totam.',
                    'image' => 'W68cyVAnSSoLqzQedjeUCfGUulkXUanb3g7rNLQp.png',
                    'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laborum dolore odio animi ipsum recusandae beatae quos neque.',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Labore.',
                    'image' => 'UesIlxatVNlGw07ZcJ13l8b2H0hog9OsZKOYp0li.png',
                    'content' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et dolor officiis nisi quis laborum sunt eius? Eius, minima dolores!',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'title' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero odio quidem ea eaque dolor commodi autem qui sequi?',
                    'image' => 'Os8dmWUDok0EwlEBfPzulPYD8znLrWS5UZHi9WBT.png',
                    'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus eius ipsum odit non, ullam excepturi ex nesciunt explicabo corporis quasi officia optio vitae similique velit hic, voluptatem quisquam vel deserunt quaerat magnam? Voluptate nesciunt laborum et, accusantium est earum atque minus sint, quo, odit rem',
                    'created_at' => date('Y-m-d H:i:s')


                ]
            ];

        Post::insert($dataPost);
    }
}
